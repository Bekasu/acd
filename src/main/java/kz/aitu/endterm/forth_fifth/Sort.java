package kz.aitu.endterm.forth_fifth;

public class Sort {
    public void sort(int[] arr){
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; i < arr.length - i - 1; j++){
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }

    private void sort(String arr){
        for (int i = 0; i < arr.length(); i++){
            for (int j = 0; i < arr.length() - i - 1; j++){
                String temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
    public boolean anogram(String s, String t){
        sort(s);
        sort(t);
        if (s==t) return true;
        else return false;
    }
}
