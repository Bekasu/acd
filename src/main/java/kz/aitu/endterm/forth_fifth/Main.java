package kz.aitu.endterm.forth_fifth;

import java.util.Scanner;

public class Main {
    private static int lastOccur(int[] arr, int searched){
        int index;
        int mid = arr.length/2;
        int last = arr.length;
        for (int i = 0; i < last; i++){
            while (mid < last){
                  if (arr[mid] == searched) break;
                  if (arr[mid] < searched) mid = (last - mid)/2;
                  if (arr[mid] > searched){
                      last = mid;
                      mid = mid/2;
                  }
            }
        }
        for (int i = mid; i < arr.length; i++){
            if (arr[i] == arr[mid]) mid = i;
        }
        return mid;
    }
    public static void main(String[] args) {
        Sort st = new Sort();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int arr[] = new int[n];
        for (int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        System.out.println("Enter the searched number--------------");
        int searched = sc.nextInt();

        System.out.println("------Enter ur anograms-------");
        String string1 = sc.next();
        String string2 = sc.next();

        st.sort(arr);
        lastOccur(arr, searched);

    }

}
