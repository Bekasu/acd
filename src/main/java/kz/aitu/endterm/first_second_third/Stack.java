package kz.aitu.endterm.first_second_third;

public class Stack {
    private StackNode top;

    public void push(StackNode node){
        if (top == null) top = node;
        else {
            top.setNext(node);
            top = node;
        }
    }
    //int 'cause to find the total sum
    public int pull(){
        int value = top.getValue();
        top = top.getNext();
        return value;
    }

    public StackNode getTop() {
        return top;
    }

    public void setTop(StackNode top) {
        this.top = top;
    }
}
