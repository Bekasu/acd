package kz.aitu.week2;

import java.util.Scanner;

public class problem707 {
    public void permutation(String s, String str){
        int n = str.length();
        if (n == 0) System.out.println(s);
        else {
            for (int i = 0; i < n; i++){
                permutation(s+str.charAt(i), str.substring(0,i)+str.substring(i+1,n));
            }
        }
    }

    public void run(){
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        permutation("", str);

    }
}
