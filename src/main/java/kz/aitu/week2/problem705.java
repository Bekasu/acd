package kz.aitu.week2;

import java.util.Scanner;

public class problem705 {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int k = sc.nextInt();
    int arr[] = new int[k];

    public void sequences(int arr[], int n, int k, int z){
        if (n == 0) {
            for( int i = 0; i < z; i++)
                System.out.print(arr[i] + " ");
                System.out.println();
        }

        if (n > 0) {
            for(int i = 1; i<=k; ++i) {
                arr[z] = i;
                sequences(arr, n-1, k, z+1);
            }
        }
    }

    public void run(){
        sequences(arr,n,k,0);
    }
}
