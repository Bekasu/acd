package kz.aitu.week2;

import java.util.Scanner;

public class problem702 {

    public void reverse(int n){
        if (n == 0)return ;
        else {
            Scanner s = new Scanner(System.in);
            int a = s.nextInt();
            reverse(n - 1);
            System.out.println(a + " ");
        }
    }

    public void run(){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        reverse(n);
    }
}
