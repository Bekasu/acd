package kz.aitu.week2;

import java.util.Scanner;

public class problem703 {

    public void inverse(int n){
        if (n == 0) return;
        else {
            Scanner s = new Scanner(System.in);
            String str = s.next();
            inverse(n-1);
            System.out.println(str);
        }
    }

    public void run(){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        inverse(n);

    }
}
