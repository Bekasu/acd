package kz.aitu.week2;

import java.util.Scanner;

public class problem701 {

    public int fibonacci(int a) {
        if (a == 1 || a == 0) return a;

        return fibonacci(a-2) + fibonacci(a-1);
    }

    public void run() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(fibonacci(n));
    }
}