package kz.aitu.progs;

public class Vertex<Key,Value> {
    private Key key;
    private Value value;
    private LinkedList<Key,Value> edges;
    private Vertex next;
    private Vertex nextLL;

    public Vertex(Key key,Value value) {
        this.key = key;
        this.value = value;
        edges = new LinkedList();
    }

    public void addEdge(Vertex vertex){
        if (edges.hasEdge(vertex)) return;
        else {
            edges.add(vertex);
        }
    }

    public void setNextLL(Vertex nextLL) {
        this.nextLL = nextLL;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public void setEdges(LinkedList<Key,Value> edges) {
        this.edges = edges;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Vertex getNextLL() {
        return nextLL;
    }

    public Vertex getNext() {
        return next;
    }

    public Value getValue() {
        return value;
    }

    public Key getKey() {
        return key;
    }

    public LinkedList<Key,Value> getEdges() {
        return edges;
    }
}
