package kz.aitu.progs;

public class Hashtable<Key,Value> {
    private int size = 11;
    private Vertex<Key,Value>[] table = new Vertex[size];

    public boolean containsKey(Key key){
        Vertex current = table[key.hashCode()%size];
        while (current != null){
            if (current.getKey() == key) return true;
            current = current.getNext();
        }
        return false;
    }
    public void put(Vertex vertex){
        Vertex current = table[vertex.getKey().hashCode()%size];
        if (current == null) {
            current = vertex;
            return;
        }
        while (current.getNext() != null){
            current = current.getNext();
        }
        current.setNext(vertex);
    }

    public Vertex getVertex(Key key){
        if (containsKey(key)){
            Vertex vertex = table[key.hashCode()%size];
            while (vertex != null){
                if (vertex.getKey() == key) return vertex;
                vertex = vertex.getNext();
            }
        }
        return null;
    }
}
