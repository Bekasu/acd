package kz.aitu.progs;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph<Integer,String>();
        graph.addVertex(1,"Aslan");
        graph.addVertex(2,"Alish");
        graph.addVertex(3,"Beka");
        graph.addVertex(4,"Arman");
        graph.addVertex(5,"Temir");

        graph.addEdge(1,2,true);
        graph.addEdge(1,3,true);
        graph.addEdge(1,4,true);
        graph.addEdge(1,5,true);
        graph.addEdge(1,2,true);

    }
}
