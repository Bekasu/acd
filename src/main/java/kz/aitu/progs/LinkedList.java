package kz.aitu.progs;

public class LinkedList<Key,Value> {
    private Vertex head;
    private Vertex tail;

    public void add(Vertex vertex){
        if (head == null) {
            head = vertex;
            tail = vertex;
        } else {
            tail.setNext(vertex);
            tail = vertex;
        }
    }

    public boolean hasEdge(Vertex vertex){
        if (head == null) return false;
        Vertex current = head;
        while (current != null){
            if (current == vertex) return true;
            current = current.getNextLL();
        }
        return false;
    }

    public Vertex getHead() {
        return head;
    }

    public void setHead(Vertex head) {
        this.head = head;
    }

    public Vertex getTail() {
        return tail;
    }

    public void setTail(Vertex tail) {
        this.tail = tail;
    }
}
