package kz.aitu.progs;

public class Graph<Key,Value> {
    private boolean bi;
    private Hashtable<Key,Value> Htable = new Hashtable();

    public void addVertex(Key key,Value value){
        Vertex vertex = new Vertex<Key,Value>(key,value);

        if (Htable.containsKey(key)) return;
        else Htable.put(vertex);
    }

    public void addEdge(Key k1, Key k2, boolean bi){
        Vertex v1 = Htable.getVertex(k1);
        Vertex v2 = Htable.getVertex(k2);
        if (v1 != null && v2 != null) {
            if (bi) {
                v1.addEdge(v2);
                v2.addEdge(v1);
            } else {
                v1.addEdge(v1);
            }
        }
    }
}
