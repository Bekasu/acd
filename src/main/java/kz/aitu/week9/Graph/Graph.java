package kz.aitu.week9.Graph;

public class Graph<Key, Value> {
    private boolean bidirectional;
    private  Hashtable<Key, Value> vertexHashtable = new Hashtable<Key,Value>();

    public void addVertex(Key key, Value value){
        Vertex vertex = new Vertex<Key, Value>(key, value);

        if (vertexHashtable.containsKey(key)) return;
        else vertexHashtable.put(key, vertex);

    }
    public void addEdge(Key key1, Key key2, boolean bidirectional){
        Vertex vertexA = vertexHashtable.get(key1);
        Vertex vertexB = vertexHashtable.get(key2);

        vertexA.addEdge(vertexB);
        if(bidirectional) vertexB.addEdge(vertexA);

    }

    public void countVertex(){
        System.out.println(vertexHashtable.countVertex());
    }

    public  void countEdge(Key key){
        Vertex vertex= vertexHashtable.get(key);
        System.out.println(vertex.countEdge());

    }


}
