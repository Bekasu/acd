package kz.aitu.week9.Graph;

import kz.aitu.week3.quiz.Node;

public class LinkedList<Key,Value> {
    private Vertex head;
    private Vertex tail;

    public boolean contains(Vertex vertex){
        Vertex current = head;
        while(current!= null) {
            if (current == vertex) return true;
            current = current.getNextLL();
        }
        return false;
    }
    public int countEdge(){
        int count = 0;
        Vertex current = head;
        while (current != null) {
            count++;
            current = current.getNextLL();
        }
        return count;
    }
    public void add(Vertex vertex){
        if (head == null){
            head = vertex;
            tail = vertex;
        } else {
            tail.setNext(vertex);
            tail = vertex;
        }
    }


    public Vertex getHead() {
        return head;
    }

    public Vertex getTail() {
        return tail;
    }

    public void setTail(Vertex tail) {
        this.tail = tail;
    }

    public void setHead(Vertex head) {
        this.head = head;
    }
}
