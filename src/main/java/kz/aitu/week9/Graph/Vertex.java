package kz.aitu.week9.Graph;

import kz.aitu.week6.hash.Node;

import java.util.Objects;

public class Vertex<Key,Value> {
    private Key key;
    private Value value;
    private LinkedList<Key,Value> edges;
    private Vertex next;
    private Vertex nextLL;

    public Vertex(Key key, Value value) {
        this.key = key;
        this.value = value;
        edges = new LinkedList<>();
    }

    public void addEdge(Vertex vertex){
        if(!edges.contains(vertex)) edges.add(vertex);
    }

    public int countEdge(){
        return edges.countEdge();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        Vertex vertex = (Vertex) o;
        if (this.getKey() == vertex.getKey()){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    public Key getKey() {
        return key;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public void setEdges(LinkedList<Key, Value> edges) {
        this.edges = edges;
    }

    public LinkedList<Key, Value> getEdges() {
        return edges;
    }

    public Vertex getNext() {
        return next;
    }

    public Vertex getNextLL() {
        return nextLL;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public void setNextLL(Vertex nextLL) {
        this.nextLL = nextLL;
    }
}
