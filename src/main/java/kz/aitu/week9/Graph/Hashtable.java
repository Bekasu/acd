package kz.aitu.week9.Graph;

import kz.aitu.week6.hash.Node;

public class Hashtable<Key, Value> {
    private int size = 11;
    private Vertex<Key, Value> table[] = new Vertex[size];

    public boolean containsKey(Key key){
        if (table[key.hashCode()%size] != null) {
            Vertex vertex = table[key.hashCode() % size];
            while (vertex != null){
                if (vertex.getKey() == key) return true;
                vertex = vertex.getNext();
            }
        }
        return false;
    }
    public Vertex get(Key key){
        if (table[key.hashCode()%size] != null) {
            Vertex vertex = table[key.hashCode() % size];
            while (vertex != null){
                if (vertex.getKey() == key) return vertex;
                vertex = vertex.getNext();
            }
        }
        return null;
    }
    public int countVertex(){
        int count = 0;
        for (int i = 0; i < size; i++){
            Vertex vertex = table[i];
            while (vertex != null){
                count++;
                vertex = vertex.getNext();
            }
        }
        return count++;
    }

    public int index(Vertex vertex){
        int n = vertex.getKey().hashCode()%size;
        return n;
    }

    public void put(Key key, Vertex vertex){
        int index = index(vertex);

        if (table[index] == null) {
            table[index] = vertex;

        } else {
            Vertex current = table[index];
            while (current != null) {
                if (current.equals(vertex)){
                    current.setValue(vertex.getValue());
                    return;
                }
                if (current.getNext() == null){
                    current.setNext(vertex);
                    return;
                }
                current = current.getNext();
            }
        }
    }
}
