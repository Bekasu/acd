package kz.aitu.week7;

public class Insertion {
    public void sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j != -1; j--) {
                if (arr[j] > arr[i]) {
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                    i = j;
                }
            }
        }
    }
}
