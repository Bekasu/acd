package kz.aitu.week1;

import java.util.Scanner;

public class Practice5 {
    public static int factorial(int a){
        if ((a == 1) || (a == 0)) {
            return a;
        } else  {
            return factorial(a-1) + (factorial(a-2));
        }
    }
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int n = sc.nextInt();

        System.out.println(factorial(n));
    }
}