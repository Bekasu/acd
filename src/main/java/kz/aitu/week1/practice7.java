package kz.aitu.week1;
import java.util.Scanner;
public class practice7 {
    static void reverse(int a){
        if (a==0) {
            return;
        }
        else{
            Scanner sc= new Scanner(System.in);
            int n = sc.nextInt();
            reverse(a-1);
            System.out.println(n + " ");
        }
    }
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int a = sc.nextInt();
        reverse(a);
    }
}
