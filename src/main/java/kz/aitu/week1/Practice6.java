package kz.aitu.week1;

import java.util.Scanner;

public class Practice6 {
    public static int factor(int a, int n){
        if (n==0) {
            return 1;
        }
        else{
            return a * factor(a,n-1);
        }
    }
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int a = sc.nextInt();
        int n = sc.nextInt();
        System.out.println(factor(a,n));
    }
}