package kz.aitu.week1.quiz;

import java.util.Scanner;

public class quiz2 {
    public static int max(int n){
        Scanner sc = new Scanner(System.in);
        int maxi = n;
        int inp;
        if (n!=0){
            inp = sc.nextInt();
            if (inp >= n){
                maxi = inp;
                return max(inp);
            }
        }
        return maxi;

    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        System.out.println(max(n));
    }
}
