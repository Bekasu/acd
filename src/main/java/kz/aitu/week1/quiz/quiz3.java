package kz.aitu.week1.quiz;

import java.util.Scanner;

public class quiz3 {
    public static int second_maximum(int max1, int max2) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        if (a == 0) return max2;
        if (a > max1) {
            max2 = max1;
            max1 = a;
        }
        else if (a > max2) max2 = a;

    return second_maximum(max1, max2);
    }

    public static void main (String[] args){
        System.out.println(second_maximum(0,0));
    }
}
