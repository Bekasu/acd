package kz.aitu.week1.quiz;

import java.util.Scanner;

public class quiz1 {
    public static int output(int a, int n){
        System.out.println(n);
        if (a!=n) {
            n++;
            return output(a,n);
        }
        return 0;
    }
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int a = sc.nextInt();
        int n=1;
        output(a,n);
    }
}
