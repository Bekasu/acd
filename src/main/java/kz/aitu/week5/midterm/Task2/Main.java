package kz.aitu.week5.midterm.Task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Stack stack1 = new Stack();
        Stack stack2 = new Stack();

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++){
            int a = sc.nextInt();
            stack1.push(new Node(a));
        }

        for (int i = 0; i < n; i++){
            int b = sc.nextInt();
            stack2.push(new Node(b));
        }

        stack1.merge(stack1,stack2);
    }
}
