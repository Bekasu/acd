package kz.aitu.week5.midterm.Task2;

public class Stack {
    private Node top;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public void push(Node node){
        if (top == null) {
            top = node;
            size++;
        }
        else {
            node.setNext(top);
            top = node;
            size++;
        }
    }

    public Node pop(){
        Node node = top;
        top = top.getNext();
        size--;
        return node;
    }

    public void merge(Stack stack1, Stack stack2){
        int arr[] = new int[100];
        Node st1 = stack1.getTop();
        for (int i = 0; i<stack1.getSize();i++){
            arr[i] = st1.getValue();
            st1 = st1.getNext();
        }
        Node st2 = stack2.getTop();
        for (int i = stack1.getSize(); i<stack2.getSize(); i++){
            arr[i] = st2.getValue();
            st2 = st2.getNext();
        }

    }

}
