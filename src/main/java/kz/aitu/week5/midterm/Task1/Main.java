package kz.aitu.week5.midterm.Task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        LinkedList ls = new LinkedList();

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        //O(n) - loop that runs n times
        for (int i = 0;i < n;i++){
            int a = sc.nextInt();
            ls.addNode(new Node(a));
        }
        Node node = new Node(12);
        ls.push_back(node);
        System.out.println("----------PRINT----------");
        ls.print(ls.getHead());
    }
}
