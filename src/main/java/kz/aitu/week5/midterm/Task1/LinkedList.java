package kz.aitu.week5.midterm.Task1;

public class LinkedList {
    private Node head;
    private Node tail;

    public Node getTail() {
        return tail;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }
    //O(n) - because loop is used
    public void push_back(Node node){
        if (head == null)
        {
            head = node;
            return;
        }
        node.setNext(null);
        Node last = head;
        while (last.getNext() != null)
            last = last.getNext();

        last.setNext(node);
        return;
    }
    //O(1)
    public void addNode(Node node){
        if (head == null){
            head = node;
            tail = node;
        } else {
            tail.setNext(node);
            tail = node;
        }
    }
    //O(n) - recursion used
    public void print(Node node){
            if (node == null)
                return;
            else {
                System.out.print(node.getValue()+ " ");
                print(node.getNext());
            };
        }
    }

