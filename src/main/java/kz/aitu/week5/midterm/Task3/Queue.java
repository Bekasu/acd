package kz.aitu.week5.midterm.Task3;

public class Queue {
    private Node head;
    private Node tail;

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void push(Node node){
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            tail.setNext(node);
            tail = node;
        }
    }
    public Node pop(){
        Node node = head;
        head = head.getNext();
        return node;
    }

    //O(n) - it recurse itself, in reality it is O(n + 1)
    public void printOdd(Node node){
        if (node == null) return;
        else {
            int length = node.getValue().length();
            if (length%2 == 1) {
                System.out.print(node.getValue() + " ");
            }
            printOdd(node.getNext());
        }
    }
}
