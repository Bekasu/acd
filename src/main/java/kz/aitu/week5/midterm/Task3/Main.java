package kz.aitu.week5.midterm.Task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Queue queue = new Queue();

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++){
            String str = sc.next();
            queue.push(new Node(str));
        }
        queue.push(new Node("asd"));
        queue.pop();
        System.out.println("----------PRINT_ODD_ELEMENTS---------");
        queue.printOdd(queue.getHead());
    }
}
