package kz.aitu.week5.additional;

public class Stack {
    private Node top;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public void push(Node node){
        if (top == null) {
            top = node;
            size++;
        }
        else {
            node.setNext(top);
            top = node;
            size++;
        }
    }
    public void pop(){
        top = top.getNext();
        size--;
    }
    public Stack merge(Stack st1, Stack st2, Stack st3, Stack st4){
        Node st1_head = st1.getTop();
        Node st2_head = st2.getTop();
        Node st3_head = st3.getTop();
        if ((st1_head == null) && (st2_head == null) && (st3_head == null)) return st4;

        if ((st1_head.getValue() > st2_head.getValue()) && (st1_head.getValue() > st3_head.getValue())){
            st4.push(st1_head);
            st1.pop();
        }
        if ((st2_head.getValue() > st1_head.getValue()) && (st2_head.getValue() > st3_head.getValue())){
            st4.push(st2_head);
            st2.pop();
        }
        if ((st3_head.getValue() > st2_head.getValue()) && (st3_head.getValue() > st1_head.getValue())){
            st4.push(st3_head);
            st3.pop();
        }
        return merge(st1,st2,st3,st4);
    }


    public void print(){
        Node node = top;
        while (node != null){
            System.out.println(node);
            node.getNext();
        }
    }
}
