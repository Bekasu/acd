package kz.aitu.week5.additional;

public class Main {
    public static void main(String[] args) {
        Stack Stack1 = new Stack();
        Stack Stack2 = new Stack();
        Stack Stack3 = new Stack();
        Stack Stack4 = new Stack();

        Stack1.push(new Node(1));
        Stack1.push(new Node(2));
        Stack1.push(new Node(3));
        Stack1.push(new Node(4));
        Stack1.push(new Node(5));

        Stack2.push(new Node(1));
        Stack2.push(new Node(11));
        Stack2.push(new Node(12));
        Stack2.push(new Node(40));
        Stack2.push(new Node(41));

        Stack3.push(new Node(2));
        Stack3.push(new Node(4));
        Stack3.push(new Node(5));
        Stack3.push(new Node(6));
        Stack3.push(new Node(7));


        Stack4.merge(Stack1,Stack2,Stack3,Stack4);
        Stack4.print();
    }
}
