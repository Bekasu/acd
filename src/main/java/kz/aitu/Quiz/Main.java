package kz.aitu.Quiz;

import java.util.Scanner;

public class Main {
    static int[] quickSort(int[] arr) {
        int p = arr[0];
        int[] right = new int[5];
        int[] left = new int[5];
        int[] equal = new int[5];

        int equalI = 0;
        int rightIndex = 0;
        int leftIndex = 0;
        for (int i = 0; i < arr.length; i++){
            if (arr[i] > p) {
                right[rightIndex] = arr[i];
                rightIndex++;
            } else if (arr[i] == p){
                equal[equalI] = arr[i];
                equalI++;
            } else {
                left[leftIndex] = arr[i];
                leftIndex++;
            }
        }
        int[] result = new int[arr.length];
        for(int i = 0; i < left.length; i++){
            result[i] = left[i];
        }
        equalI = 0;
        for (int i = left.length; i < left.length + equal.length; i++){
            result[i] = equal[equalI];
            equalI++;
        }
        rightIndex = 0;
        for(int i = left.length + equal.length; i < arr.length; i++){
            result[i]= right[rightIndex];
            rightIndex++;
        }

        return result;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Quick quick = new Quick();
        int[] arr = new int[10];
        for (int i = 0; i < 10; i++){
            arr[i] = sc.nextInt();
        }


        quick.print(arr);
    }
}
