package kz.aitu.Quiz;

public class Quick {
    public void sort(int[] arr){
        quickSort(arr, 0, arr.length - 1);
    }
    private void quickSort(int[] arr, int first, int last){
        if (first < last){
            int j = first - 1;
            for (int i = first; i < last; i++){
                j++;
                if (arr[i] > arr[last]){
                    int temp = arr[i];
                    arr[i] = arr[last];
                    arr[j] = temp;
                }
            }
            j++;
            int temp = arr[j];
            arr[j] = arr[last];
            arr[last] = temp;
            quickSort(arr, first, j - 1);
            quickSort(arr, j + 1, last);
        } else return;
    }

    public void print(int[] arr){
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
    }
}
