package kz.aitu.week3.quiz;

public class Main {

    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();
        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));

        //finding middle element of LinkedList in single pass
        Node middle; //write your code here
        Node temp = head;
        int length = 0;
        while (temp !=  null){
            length++;
            temp = temp.next();
        }
        middle = head;

        for (int i = 0; i < length/2; i++){
            middle = middle.next();
        }



        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : " + middle);

    }
}
