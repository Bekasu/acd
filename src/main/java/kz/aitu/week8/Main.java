package kz.aitu.week8;

import java.util.Random;
import java.util.Scanner;

public class Main {
    private static void printAll(int[] array) {
        System.out.print("==> ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();
    }
    public static void main(String[] args) {
        Random random = new Random();
        BinarySearch bs = new BinarySearch();
        JumpSearch js = new JumpSearch();
        LinearSearch ls = new LinearSearch();
        Sum sum = new Sum();

        int[] arr = new int[20];
        for (int i = 0; i < 20; i++){
            arr[i] = random.nextInt(10);
        }

        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        printAll(arr);
//        sum.sumOfNum(arr,10);
//        ls.search(arr, num);


    }
}
