package kz.aitu.week8;

public class Sum {
    public void sumOfNum(int[] arr, int num){
        int inf = 99999;
        int[] arr2 = new int[num+1];
        arr2[0] = 0;
        int md, i;
        for (md = 1; md <= num; md++){
            arr2[md] = inf;
            for (i = 0; i < 20; i++){
                if (md >= arr[i] && arr2[md - arr[i]] + 1 < arr2[md]) arr2[md] = arr2[md-arr[i]]+1;
            }
        }
        if (arr2[num] == inf) System.out.println("No solution");
        else while (num > 0) {
            for (i = 0; i < 20; i++){
                if (arr2[num - arr[i]] == arr2[num]){
                    System.out.println(arr[i] + " ");
                    num -= arr[i];
                    break;
                }
            }
        }
    }
}
