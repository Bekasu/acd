package kz.aitu.week4.Stack;

public class Stack {
    private Node top;
    private int size = 0;

    public void setTop(Node top) {
        this.top = top;
    }

    public Node getTop() {
        return top;
    }

    public void Push(int value){
        Node new_node = new Node(value);
        if (top == null) {
            top = new_node;
            size++;
        }
        else {
            new_node.setNextTop(top);
            top = new_node;
            size++;
        }
    }
    public void Pop(){
        top = top.getNextTop();
        size--;
    }

    public boolean empty(){
        if (top == null) return true;
        else return false;
    }
    public int size(){

        return size;
    }
}
