package kz.aitu.week4.Stack;

public class Node {
    private int value;
    private Node nextTop;

    public Node(int value) {

        this.value = value;
    }

    public void setNextTop(Node nextTop) {

        this.nextTop = nextTop;
    }

    public void setValue(int value) {

        this.value = value;
    }

    public int getValue() {

        return value;
    }

    public Node getNextTop() {

        return nextTop;
    }
}
