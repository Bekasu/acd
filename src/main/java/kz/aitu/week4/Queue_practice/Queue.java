package kz.aitu.week4.Queue_practice;


public class Queue {
    private Node head;
    private Node tail;

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public Node getHead() {
        return head;
    }
    public void add(Node node){
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            tail.setNext(node);
            tail = node;
        }
    }
    public int size(){
        Node node = head;
        int c = 0;
        if (head == null) return 0;
        else {
            while (node != null){
                c++;
                node = node.getNext();
            }
        }
        return c;
    }
    public Node poll(){
        Node node = head;
        head = head.getNext();
        return node;
    }
    public Node peek(){
        if (head == null) return null;
        else {
            return head;
        }
    }
    public boolean empty(){
        if (head == null) return true;
        else return false;
    }
}
