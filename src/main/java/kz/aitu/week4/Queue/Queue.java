package kz.aitu.week4.Queue;

public class Queue {
    private Node head;

    private Node tail;

    public Node getHead()
    {
        return head;
    }

    public void setHead(Node head) {

        this.head = head;
    }

    public Node getTail() {

        return tail;
    }

    public void setTail(Node tail) {

        this.tail = tail;
    }

    public void add(String value){
        Node node = new Node(value);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
          tail.setNext(node);
          tail = node;
        }

    }
    public int size(){
        Node node = head;
        int size = 0;
        if (head == null) return 0;
        else {
            while (node != null) {
                node = node.getNext();
                size++;
            }
        }
        return size;
    }
    public Node poll(){
        Node node = head;
        if (head == null) return null;
        else {
            head = head.getNext();
            return node;
        }
    }

    public  Node peek(){
        Node node = head;
        if (head == null) return null;
        else {
            return node;
        }
    }
    public boolean empty(){
        if (head == null) return true;
        else return false;
    }
}
