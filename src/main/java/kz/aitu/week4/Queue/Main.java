package kz.aitu.week4.Queue;

public class Main {

    public static void main(String[] args) {
        Queue queue = new Queue();

        queue.add("Iphone");
        queue.add("Sumsung");
        queue.add("Nokia");
        queue.add("Xiomi");
        queue.add("Huawei");

        System.out.println(queue.size());
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.size());
        System.out.println(queue.empty());

    }
}
