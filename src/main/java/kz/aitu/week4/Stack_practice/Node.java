package kz.aitu.week4.Stack_practice;

public class Node  {
    private String value;
    private Node next;

    public Node(String value) {
        this.value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue()  {
        return value;
    }
}
