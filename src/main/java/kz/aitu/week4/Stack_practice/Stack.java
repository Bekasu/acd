package kz.aitu.week4.Stack_practice;

public class Stack {
    private Node top;

    public void setTop(Node top) {
        this.top = top;
    }

    public Node getTop() {
        return top;
    }

    public boolean empty(){
        if (top == null) return true;
        else return false;
    }

    public void push(Node node){
        if (top == null) {
            top = node;
        }
        else {
            node.setNext(top);
            top = node;
        }
    }
    public void pop(){
        top = top.getNext();
    }
    public void print(){
        Node node = top;
        while (node != null){
            System.out.println(node.getValue());
            node = node.getNext();
        }
    }

}
