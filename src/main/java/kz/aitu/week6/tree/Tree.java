package kz.aitu.week6.tree;

public class Tree {
    private Node root;

    public void put(int key, String value)
    { root = put(root, key, value); }

    //function to insert
    private Node put(Node node, int key,String value)
    {
        if (node == null) return new Node(key, value);
        int cmp =  key - (node.getKey());
        if (cmp < 0)
            node.setLeft(put(node.getLeft(), key, value));
        else if (cmp > 0)
            node.setRight(put(node.getRight(), key, value));
        else if (cmp == 0)
            node.setValue(value);
        return node;
    }

    //function to find by key
    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }
    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;

    }public void printAll() {
        Queue newqueue = new Queue();
        newqueue.push(root.getKey());
        printingAll(root,newqueue);
    }

    private void printingAll(Node root, Queue queue){
        while (!queue.isEmpty()) {
            Node current=root;
            int a = queueprint(queue);
            while (current!=null){
                if (a < current.getKey()){
                    current = current.getLeft();
                } else if (a>current.getKey()){
                    current = current.getRight();
                } else if (a==current.getKey()){
                    break;
                }
            }
            queuepush(current.getLeft(), queue);
            queuepush(current.getRight(), queue);
        }
    }

    private void queuepush(Node root, Queue queue){
        if (root!=null) {
            queue.push(root.getKey());
        }
    }

    private int queueprint(Queue queue){
        int rot = queue.pop();
        Node current = root;
        while (current!=null){
            if (rot < current.getKey()){
                current = current.getLeft();
            } else if (rot>current.getKey()){
                current = current.getRight();
            } else {
                System.out.print(current.getValue()+" ");
                break;
            }
        }
        return rot;
    }

    //function to print in ascending order
    public void printAllAscending() {
      printAsc(root);
    }

    private void printAsc(Node node) {
        if(node != null) {
            printAsc(node.getLeft());
            System.out.print(node.getValue());
            printAsc(node.getRight());
        }
    }
    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
}
