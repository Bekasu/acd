package kz.aitu.week6.tree;

public class Main {
    public static void main(String[] args) {
        Tree bsTree = new Tree();

        bsTree.put(1000, "A");
        bsTree.put(2000, "B");
        bsTree.put(500, "C");
        bsTree.put(1500, "D");
        bsTree.put(750, "E");
        bsTree.put(250, "F");
        bsTree.put(625, "G");
        bsTree.put(1250, "H");
        bsTree.put(875, "I");
        bsTree.put(810, "Z");

        System.out.println("===Print 810 (Z)");
        System.out.println(bsTree.find(810));
        System.out.println("===Print ALLAscending(FCGEZIAHDB)");
        bsTree.printAllAscending();

     System.out.println("===Print ALL(ACBFEDGIHZ)");
        bsTree.printAll();
        System.out.println("===Removing 1000");
        System.out.println("===Print ALLAscending(FCGEZIHDB)");
        bsTree.printAllAscending();
        System.out.println("===Print ALL(ICBFEDGZH)");
        bsTree.printAll();

    }

}
