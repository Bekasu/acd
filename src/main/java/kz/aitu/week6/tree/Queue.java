package kz.aitu.week6.tree;

public class Queue {
    private QueueNode head;
    private QueueNode tail;

    public void push(int key){
        QueueNode newNode = new QueueNode(key);
        if (head==null){
            head=newNode;
            tail=newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;
        }
    }

    public void print(){
        QueueNode current = head;
        while (current!=null){
            System.out.print(current+" ");
            current = current.getNext();
        }
    }

    public int pop(){
        QueueNode current = head;
        head = head.getNext();
        return current.getKey();
    }

    public boolean isEmpty(){
        return head==null;
    }

    public void deleteAllItems(){
        head = null;
    }

}
