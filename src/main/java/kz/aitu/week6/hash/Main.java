package kz.aitu.week6.hash;

public class Main {
    public static void main(String[] args) {
        Hashtable hashtable = new Hashtable();
        hashtable.put(1, "Batman");
        hashtable.put(2, "Superman");
        hashtable.put(3, "WonderWomen");
        hashtable.put(4, "Aquaman");
        hashtable.put(5, "GreenLantern");
        hashtable.put(6, "Cyborg");
        hashtable.put(7, "Phoenix");
        hashtable.put(8, "Catgirl");
        hashtable.put(9, "Penguin");
        hashtable.put(10, "Joker");
        hashtable.put(11, "Joker");
        System.out.println("hashtable:");
        hashtable.print();
        System.out.println("------------------------");
        hashtable.get(3);
        System.out.println("------------------------");
        hashtable.remove(11);

    }
}
