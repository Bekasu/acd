package kz.aitu.week6.hash;

import java.util.Objects;

public class Node extends Object{
    private int key;
    private String value;
    private Node next;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        Node node = (Node) o;
        if (this.getKey() == node.getKey()){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() { return Objects.hash(key); }

    public Node(int key, String value) {
        this.key = key;
        this.value = value;
        this.next = null;
    }

    public int getKey() {
        return key;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public String getValue() {
        return value;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
