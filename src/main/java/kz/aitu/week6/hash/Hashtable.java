package kz.aitu.week6.hash;

import java.util.Objects;

public class Hashtable extends Object{
    private int size = 10;
    private Node array[] = new Node[size];
    private double loadFactor = 0.75;
    private int initialCapacity = 0;

    public int index(Node node){
        int n = node.hashCode()%size;
        return n;
    }

    public void put(int key, String value){
        Node node = new Node(key, value);
        int index = index(node);

        if (array[index] == null) {
            array[index] = node;

        } else {
            Node current = array[index];
            while (current != null) {
                if (current.equals(node)){
                    current.setValue(node.getValue());
                    return;
                }
                if (current.getNext() == null){
                    current.setNext(node);
                    return;
                }
                current = current.getNext();
            }
        }
    }

    public void get(int key){
        Node new_node = new Node(key,"");
        for (int i = 0; i < size; i++) {
            Node node = array[i];
            while(node != null) {
                if(node.equals(new_node)) {
                    System.out.println(node.getValue());
                    return;
                }
                node = node.getNext();
            }
        }
    }

    public void print() {
        for(int i = 0; i < size; i++) {
            if (array[i] == null) continue;
            Node current = array[i];
            while(current != null){
                System.out.print(current.getValue() + " ");
                current = current.getNext();
            }
            System.out.println();
        }
    }

    public void remove(int key) {
        Node kk = new Node(key, null);
        Node node = array[index(kk)];
        if (node.getNext() == null){
            if (node.getKey() == kk.getKey()) {
                System.out.println(node.getValue());
                node = null;
            }
            return;
        }
        while (node != null) {
            if (node.getNext().getKey() == kk.getKey()) {
                System.out.println(node.getNext().getValue());
                    node.setNext(node.getNext().getNext());
                    return;
                }
        }
    }

    public double getLoadFactor() {
        return loadFactor;
    }

    public int getInitialCapacity() {
        return initialCapacity;
    }

    public void setLoadFactor(double loadFactor) {
        this.loadFactor = loadFactor;
    }

    public void setInitialCapacity(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
